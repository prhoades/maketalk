#!/usr/bin/env python

### Import libraries ###

import os
import sys
import pexpect
import re
import wave
import numpy
import math
import tempfile
from optparse import OptionParser

### END Import libraries ###

mouthlist = ""
framedir = tempfile.mkdtemp()
totalframes = 0

### This is the Gaussian data smoothing function Scott W Harden wrote ###  

def smoothListGaussian(list,degree):  
	window=degree*2-1  
	weight=numpy.array([1.0]*window)  
	weightGauss=[]  
	for i in range(window):  
		i=i-degree+1  
		frac=i/float(window)  
		gauss=1/(numpy.exp((4*(frac))**2))  
		weightGauss.append(gauss)  
	weight=numpy.array(weightGauss)*weight  
	smoothed=[0.0]*(len(list)-window)  
	for i in range(len(smoothed)):  
		smoothed[i]=sum(numpy.array(list[i:i+window])*weight)/sum(weight)  
	for i in range(window):
		smoothed.append(list[( len(list)-(window+i) )])
	return smoothed  

### End Smoothing Function ###

### This is the ramping code to keep the mouth from tending towards being all closed or tending towards being all open ###  

def ramp_mouth(frames, lenpics, rampup, rampdown):
	if rampup > 100:
		rampup = 100
	if rampdown > 100:
		rampdown = 100
	if rampup < -100:
		rampup = -100
	if rampdown < -100:
		rampdown = -100
		
	frames_ramped = []
	maxframes = max(frames)
	step = maxframes/lenpics
	if rampup >= 0:
		rampstepup = (step/100)*rampup
	else:
		rampstepup = (step/100)*(rampup*-1)
	if rampdown != 0:
		if rampdown > 0:
			rampstepdown = step-((step/100)*rampdown)
		else:
			rampstepdown = step-((step/100)*(rampdown*-1))
	else: 
		ramstepdown = 0
	# For loop ramping
	for i in frames:
		mouthindex=min(int(i/step),lenpics)
		if rampup > 0:
			if i < step and i >= (step-rampstepup):
				print "ramping frame " + str(i) + " UP " + str(rampstepup)
				i = step + (step/4)
		if rampup < 0:
			if i >= step and i <= step*2 and i <= ((step*2)-(step-rampstepup)):
				print "ramping frame " + str(i) + " CLOSED " + str(rampstepup)
				i = 0
		if rampdown > 0:
			if i >= (maxframes-step) and i <= (maxframes-rampstepdown):
				print "ramping frame " + str(i) + " DOWN " + str(rampstepdown)
				i = maxframes - step - (step/4)
		if rampdown < 0:
			if i >= (maxframes-(step*2)) and i <= (maxframes-step)-rampstepdown:
				print "ramping frame " + str(i) + " FULL OPEN " + str(rampstepup)
				i = maxframes
		if rampdown == 0 and rampup == 0:
			i = i
		if rampup == 100 and mouthindex == 0:
			print "100 ramping frame " + str(i) + " UP " + str(rampstepup)
			i = step + (step/4)
		if rampdown == 100 and mouthindex == lenpics:
			print "100 ramping frame " + str(i) + " DOWN " + str(rampstepup)
			i = maxframes - step - (step/4)
		if rampup == -100 and mouthindex == 1:
			print "100 ramping frame " + str(i) + " CLOSED " + str(rampstepup)
			i = 0
		if rampdown == -100 and mouthindex == lenpics-1:
			print "100 ramping frame " + str(i) + " UP " + str(rampstepup)
			i = maxframes
		frames_ramped.append(i)
	#End For loop ramping
	return frames_ramped  

### End ramping function ###



### Defining and parsing options ###  

parser = OptionParser()

parser.add_option("-w", "--wavefile", dest="wavefile",
                  help="Wave file to base animation on", metavar="wavefile")
parser.add_option("-p", "--picslist", dest="pics",    
                  help="List of pics to use for amplitude based lip flap", metavar="pics")
parser.add_option("-d", "--picsdir", dest="picsdir",    
                  help="Directory of pics to use must ONLY contain the pics named like 025.png 050.png 100.png etc", metavar="picsdir")
parser.add_option("-o", "--outputfile", dest="outputfile",    
                  help="Outputfile base name .mp4 or .mov will be appended", metavar="outputfile")
parser.add_option("-f", "--framerate", dest="framerate",    
                  help="Framerate to define frames per second of final video default is -f24 ", metavar="framerate")
parser.add_option("-s", "--smoothdegree", dest="smoothdegree",    
                  help="Smoothing of data pulled from wave file for animation default is -s3 ", metavar="smoothdegree")
parser.add_option("-j", "--justlist", dest="justlist",    
                  help="Only list the files as they would be animated here mostly for the GUI", metavar="justlist")
parser.add_option("-t", "--teethfreq", dest="teethfreq",    
                  help="Frequency to show teeth.png at for sibilance", metavar="teethfreq")
parser.add_option("-r", "--ramping", dest="ramping",    
                  help="THIS IS COMPLEX AND FOR ADVANCED USERS: Percentage for 'greedy' ramping -r[ramplower],[rampupper] so -r\"10,10\" would bring the top 10% of the lowest and highest frame volumes into their nearest neighbor frames while -r\"-10-10\" brings the bottom 10% and top 10% of the frames nearest to closed and full open to closed and full open.", metavar="ramping")

(options, args) = parser.parse_args()
if options.wavefile is None:   # if wavefile filename is not given
	parser.error('Wavefile Filename not given -w "wavefile.wav"')
if options.picsdir is None and options.pics is None:   # if pics list or pics dir is not given
	parser.error('No list of pics or pics dir defined')
if options.picsdir is not None:   # if pics list is not given
	options.pics = ""
if options.outputfile is None:   # if output filename is not given
	parser.error('Output filename base not given -o "outputfile_base"')
if options.framerate is None:   # if framerate is not given
	options.framerate = 24
if options.smoothdegree is None:   # if smoothdegree is not given
	options.smoothdegree = 3
if options.justlist is None:   # if justlist is not given
	options.justlist = 0

if options.ramping is None:   # if justlist is not given
	options.ramping = [0,0]
else:
	options.ramping = options.ramping.split(',')

if options.picsdir is not None:
	picsdir = options.picsdir
	#get list of pics in dir
	picstemp = sorted(os.listdir(picsdir))
	picstemp = [x for x in picstemp if x != 'teeth.png']
	pics=[picsdir + '/' + s for s in picstemp]
else:
	pics = options.pics.split(',')

if options.teethfreq is not None:
	teethfreq = int(options.teethfreq)
else:
	teethfreq = None

wavefile = options.wavefile
outputfile = options.outputfile
framerate = int(options.framerate)
smoothdegree = int(options.smoothdegree)
justlist = int(options.justlist)
rampingup = int(options.ramping[0])
rampingdown = int(options.ramping[1])
### END Defining and parsing options ###


### Other Misc Prep ###

os.system(("cp \"%s\" \"%s/working.wav\"") % (wavefile, framedir)) # work on a copy of the wav
workingwav = framedir + "/working.wav" 

#open up a wave
wf = wave.open(workingwav, 'rb')
wfswidth = wf.getsampwidth()
wfRATE = wf.getframerate()
wfchunk = int(round(wfRATE/float(framerate)))
#wfchunk = int(wfRATE/float(framerate))
wfchunkfloat = wfRATE/float(framerate)
sliprate = wfchunk-wfchunkfloat
#print "Chunk difference = " + str(wfchunk) + " - " + str(wfchunkfloat) + " " + str(sliprate)
# use a Blackman window
wfwindow = numpy.blackman(wfchunk)
wfframes=wf.getnframes()
wfduration=wfframes/float(wfRATE)
totalframes = int(round(wfduration*framerate))
slipcounter = 0
frames = []

### END Other Misc Prep ###


### The Real Mouth Work ###
max_mouthOpen = len(pics)-1


isonframe = 0

for framecounter in range(0,totalframes):
	isonframe = isonframe + 1
	if slipcounter >= wfchunk:
		slipcounter = slipcounter - wfchunk
	elif slipcounter <= (wfchunk*-1):
		wfdata = wf.readframes(wfchunk)
		wfdata = wf.readframes(wfchunk)
		slipcounter = slipcounter + wfchunk
	elif slipcounter > (wfchunk*-1) and slipcounter < wfchunk:
		wfdata = wf.readframes(wfchunk)
	if len(wfdata) == wfchunk*wfswidth:
		wfdataold = wfdata
		wfindata = numpy.array(wave.struct.unpack("%dh"%(len(wfdata)/wfswidth),wfdata))*wfwindow
		volume1 = [numpy.log10(numpy.sqrt(i)) for i in wfindata]
		volume2 = [x for x in volume1 if x > 0]
		volume = sum(volume2) / len(volume1)
		if math.isnan(volume) is True:
			volume = 0
		print "Processing frame volumes " + str(isonframe) + " " + str(volume) + " Slip Counter: " + str(slipcounter)
		frames.append(volume)
	slipcounter = slipcounter + sliprate

if rampingup != 0 or rampingdown != 0:
	frames = ramp_mouth(frames, len(pics), rampingup, rampingdown)

if smoothdegree > 0:
	frames = smoothListGaussian(frames,smoothdegree)


step = max(frames)/max_mouthOpen
#open up a wave
wf.close()
wf = wave.open(workingwav, 'rb')
wfswidth = wf.getsampwidth()
wfRATE = wf.getframerate()
wfchunk = int(wfRATE/float(framerate))
# use a Blackman window
wfwindow = numpy.blackman(wfchunk)
wfframes=wf.getnframes()
wfduration=wfframes/float(wfRATE)
#print(wfduration)

for i in range(len(frames)):
	mouth=min(int(frames[i]/step),max_mouthOpen)
	
	if i:
		if mouth>frames[i-1]+1:
			mouth=frames[i-1]+1
		elif mouth < frames[i-1]-1:
				mouth=frames[i-1]-1
	else: mouth=0
	#print frames[i]
	old_i = frames[i]
	frames[i] = mouth
	if justlist is 0:
		os.system("ln \"%s\" \"%s/frame%09d.png\"" % (pics[mouth],framedir,i))
		totalframes = i
	else:
		mouthlist += (pics[mouth])
		mouthlist += ','
	teethfilewdir = picsdir + "/teeth.png"
	if teethfreq is not None and os.path.isfile(teethfilewdir):
		maketeeth = 0
		wfdata = wf.readframes(wfchunk)
		if len(wfdata) == wfchunk*wfswidth:
			# unpack the data and times by the hamming window
			wfindata = numpy.array(wave.struct.unpack("%dh"%(len(wfdata)/wfswidth),wfdata))*wfwindow
			# Take the fft and square each value
			wffftData=abs(numpy.fft.rfft(wfindata))**2
			# find the maximum
			which = wffftData[1:].argmax() + 1
			# use quadratic interpolation around the max
			if which != len(wffftData)-1:
				y0,y1,y2 = numpy.log(wffftData[which-1:which+2:])
				x1 = (y2 - y0) * .5 / (2 * y1 - y2 - y0)
				# find the frequency and output it
				wfthefreq = (which+x1)*wfRATE/wfchunk
				if wfthefreq > teethfreq:
					#print 1
					maketeeth = 1
				else:
					#print 0
					maketeeth = 0
		if maketeeth == 1 and old_i >= (max(frames)/len(pics)):
			print "sibilance detected %i" % old_i
			if justlist is 0:
				os.system("rm \"%s/frame%09d.png\"" % (framedir,i))
				os.system("ln \"%s/teeth.png\" \"%s/frame%09d.png\"" % (picsdir,framedir,i))
				totalframes = i
			else:
				mouthlist += (pics[mouth])
				mouthlist += ','
	print "Building mouth files " + str(int(i)+1)

### END The Real Mouth Work ###


### Encoding Video ###
if justlist is 0:
	# ------------------------------
	# Then alpha mov encoding happens
	# ------------------------------
	#convcmd = "avconv -y -i %s/working.wav -r %i -i %s/frame%s.png -acodec libmp3lame -vcodec qtrle -pix_fmt argb -r %i %s.mov -r %i" % (framedir,framerate,framedir,'%9d',framerate, outputfile, framerate)
	convcmd = "avconv -y -i %s/working.wav -r %i -i %s/frame%s.png -acodec libmp3lame -vcodec qtrle -pix_fmt argb %s.mov" % (framedir,framerate,framedir,'%9d', outputfile)
	thread = pexpect.spawn(convcmd)
	cpl = thread.compile_pattern_list([
		pexpect.EOF,
		"frame= *\d+",
		'(.+)'
	])
	while True:
		iconvcmd = thread.expect_list(cpl, timeout=None)
		if iconvcmd == 0: # EOF
			print "100%"
			break
		elif iconvcmd == 1:
			frame_number = thread.match.group(0)
			frame_number = re.sub(' +',' ',frame_number)
			try:
				frame_number = frame_number.split(" ")[1]
			except:
				frame_number = frame_number.split("=")[1]
			try:
				frame_percent = (float(frame_number) / totalframes) * 100
			except:
				pass
			if frame_percent < 100:
				print ("Encoding video - %i - %i" % (float(frame_number), frame_percent)) + '%'
			thread.close
		elif iconvcmd == 2:
			pass
else:
	mouthlist = mouthlist.replace(' ', '')[:-1] #remove that trailing comma
	print "FPS %i" % framerate
	for onemouth in mouthlist.split(','):
		print onemouth #print the list

### END Encoding Video ###


### Clean Up ###
print "Deleting " + framedir
os.system(("rm -Rf %s") % framedir)

### END Clean Up ###



