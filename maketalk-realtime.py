#!/usr/bin/env python

#------
# Setting up things
#------

import commands, sys, pygame, alsaaudio, time, audioop, os
import numpy, wave
from optparse import OptionParser
import struct
parser = OptionParser()
fullscreenset = 0

parser.add_option("-f", "--framerate", dest="framerate",    
                  help="Framerate to define frames per second of final video default is -f24 ", metavar="framerate")
parser.add_option("-d", "--picsdir", dest="picsdir",    
                  help="Directory of pics to use must ONLY contain the pics named like 025.png 050.png 100.png etc", metavar="picsdir")
parser.add_option("-s", "--soundout", dest="soundout",    
                  help="Output sound from microphone to speakers -s1 or -s0", metavar="soundout")
parser.add_option("-r", "--ramping", dest="ramping",    
                  help="THIS IS COMPLEX AND FOR ADVANCED USERS: Percentage for 'greedy' ramping -r[ramplower],[ramplowerper] so -r\"10,10\" would bring the top 10% of the lowest and highest frame volumes into their nearest neighbor frames while -r\"-10-10\" brings the bottom 10% and top 10% of the frames nearest to closed and full open to closed and full open.", metavar="ramping")


(options, args) = parser.parse_args()
if options.picsdir is None and options.pics is None:   # if pics list or pics dir is not given
	parser.error('No pictures directory -d or --picsdir defined')
if options.framerate is None:   # if framerate is not given
	options.framerate = 12
if options.soundout is None:   # if framerate is not given
	options.soundout = 0

if options.ramping is None:   # if justlist is not given
	options.ramping = [0,0]
else:
	options.ramping = options.ramping.split(',')
rampingup = int(options.ramping[0])
rampingdown = int(options.ramping[1])

framerate = options.framerate
picsdir = options.picsdir
soundout = options.soundout

pygame.init()
theclock = pygame.time.Clock()
soundrate = 44100
mouth = 0
minvol = 0
#maxvol = soundrate/10
theslice = soundrate/int(framerate)
maxvol = soundrate

picstemp=sorted(os.listdir(picsdir))
picstemp = [x for x in picstemp if x != 'teeth.png']
picturelist=[picsdir + '/' + s for s in picstemp]

inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE,alsaaudio.PCM_NONBLOCK)
inp.setchannels(1)
inp.setrate(soundrate)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
inp.setperiodsize(theslice)


if soundout == 1:
	outp = alsaaudio.PCM(alsaaudio.PCM_NORMAL,alsaaudio.PCM_NONBLOCK)
	outp.setchannels(1)
	outp.setrate(soundrate)
	outp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
	outp.setperiodsize(theslice)

max_mouthOpen = len(picturelist)-1
picture = pygame.image.load(picturelist[0])
main_surface = pygame.display.set_mode(picture.get_size(),pygame.HWSURFACE|pygame.HWACCEL|pygame.DOUBLEBUF|pygame.ASYNCBLIT)
#main_surface = pygame.display.get_surface()
mouth_prev = 0
data2 = []
#-------
# End General Setup
#-------

### This is the ramping code to keep the mouth from tending towards being all closed or tending towards being all open ###  

def ramp_mouth(i, ramplower, rampupper, maxvol, mouthindex):
	if ramplower > 100:
		ramplower = 100
	if rampupper > 100:
		rampupper = 100
	if ramplower < -100:
		ramplower = -100
	if rampupper < -100:
		rampupper = -100
		
	#frames_ramped = []
	#maxframes = max(frames)
	lenpics = len(picturelist)-1
	step = maxvol/lenpics
	#print mouthindex
	if ramplower >= 0:
		rampstepup = (step/100)*ramplower
	else:
		rampstepup = (step/100)*(ramplower*-1)
	if rampupper != 0:
		if rampupper > 0:
			rampstepdown = step-((step/100)*rampupper)
		else:
			rampstepdown = step-((step/100)*(rampupper*-1))
	else: 
		ramstepdown = 0
	#print str(maxvol-step) + " " + str(maxvol-rampstepdown)
	# For loop ramping
	#print str(i) + " - " + str(maxvol) + " - " + str(step)
	if ramplower > 0:
		if i < step and i >= (step-rampstepup):
			print "LOWER ramping frame " + str(i) + " UP " + str(rampstepup)
			mouthindex = 1
	if ramplower < 0:
		if i >= step and i <= step*2 and i <= ((step*2)-(step-rampstepup)):
			print "LOWER ramping frame " + str(i) + " CLOSED " + str(rampstepup)
			mouthindex = 0
	if rampupper > 0:
		#print "YES " + str(i)
		if i >= (maxvol-step) and i <= (maxvol-rampstepdown):
			print "UPPER ramping frame " + str(i) + " DOWN " + str(rampstepdown)
			mouthindex = lenpics - 1
	if rampupper < 0:
		if i >= (maxvol-(step*(lenpics-1))) and i <= (maxvol-step)-rampstepdown:
			print "UPPER ramping frame " + str(i) + " FULL OPEN " + str(rampstepup)
			mouthindex = lenpics
	if rampupper == 0 and ramplower == 0:
			mouthindex = mouthindex
	if ramplower == 100 and mouthindex == 0:
			print "100 ramping frame " + str(i) + " UP " + str(rampstepup)
			mouthindex = 1
	if rampupper == 100 and mouthindex == lenpics:
			print "100 ramping frame " + str(i) + " DOWN " + str(rampstepup)
			mouthindex = lenpics-1
	if ramplower == -100 and mouthindex == 1:
			print "-100 ramping frame " + str(i) + " CLOSED " + str(rampstepup)
			mouthindex = 0
	if rampupper == -100 and mouthindex == lenpics-1:
			print "-100 ramping frame " + str(i) + " UP " + str(rampstepup)
			mouthindex = lenpics
	#print "POST  " + str(i) + " - " + str(maxvol) + " - " + str(step)
	return mouthindex
	#return frames_ramped  

### End ramping function ###

while True:
	l,data = inp.read()
	curr_vol = audioop.max(data, 2)


# GET FREQUENCY?
#	pitchsamps = numpy.fromstring(data, dtype=numpy.int16)
#	fourier = numpy.fft.fft(pitchsamps)
#	n = pitchsamps.size
#	timestep = 0.1
#	freqs = numpy.fft.fftfreq(n, d=timestep)
#	freq = (freq*soundrate)
#	print freqs
#	idx=numpy.argmax(numpy.abs(pitchsamps)**2)
#	freq=freqs[idx]
#	freq_in_hertz=abs(freq*soundrate/framerate)
#	print(freq_in_hertz)
## END ##

	mouthindex = min(int(curr_vol/theslice),len(picturelist))
	mouthindex = ramp_mouth(curr_vol,rampingup,rampingdown, maxvol, mouthindex)

	if mouthindex > len(picturelist)-1:
		mouthindex = len(picturelist)-1
	if soundout is not 0:
		outp.write(data)
	if mouthindex < len(picturelist)-1:
		if mouthindex < mouth_prev and mouth_prev > 1:
			#print mouth_prev
			mouth = picturelist[mouth_prev-1]
		else:
			if mouthindex > mouth_prev and mouth_prev < len(picturelist)-1:
				mouth = picturelist[mouth_prev+1]
			else:
				mouth = picturelist[mouthindex]
	else:
		mouth = picturelist[len(picturelist)-1]
	if mouthindex == mouth_prev:
		mouth = picturelist[mouthindex]

	if audioop.max(data, 2) < minvol:
		mouth = picturelist[0]
#	#print mouth
#
#	if (wfthefreq > 300):
#		mouth = picsdir + "/teeth.png"
	#print mouth
	picture = pygame.image.load(mouth)
	main_surface.blit(picture, (0, 0))

	mouth_prev = mouthindex
	for event in pygame.event.get():
		if event.type == pygame.QUIT: sys.exit()
		if event.type == pygame.KEYUP:
			if event.key == pygame.K_f:
				if fullscreenset == 0:
					fullscreenset = 1
					main_surface= pygame.display.set_mode(picture.get_size(),pygame.HWSURFACE|pygame.HWACCEL|pygame.DOUBLEBUF|pygame.FULLSCREEN)
				else:
					fullscreenset = 0
					main_surface = pygame.display.set_mode(picture.get_size(),pygame.HWSURFACE|pygame.HWACCEL|pygame.DOUBLEBUF|pygame.ASYNCBLIT)
			if event.key == pygame.K_s:
				if soundout == 0 or soundout == None:
					soundout = 1
					outp = alsaaudio.PCM(alsaaudio.PCM_NORMAL,alsaaudio.PCM_NONBLOCK)
					outp.setchannels(1)
					outp.setrate(soundrate)
					outp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
					outp.setperiodsize(theslice)
				else:
					soundout = 0
					outp.close
			if event.key == pygame.K_q:
				sys.exit()
	pygame.display.update()


