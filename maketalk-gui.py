#!/usr/bin/env python

### Import libraries ###
import sys
import pygtk
import gtk
import pexpect

maketalkguidata = '''
<?xml version="1.0" encoding="UTF-8"?>
<interface>
  <requires lib="gtk+" version="2.24"/>
  <!-- interface-naming-policy project-wide -->
  <object class="GtkWindow" id="mainwindow">
    <property name="can_focus">False</property>
    <property name="default_width">480</property>
    <property name="default_height">270</property>
    <child>
      <object class="GtkTable" id="table">
        <property name="visible">True</property>
        <property name="can_focus">False</property>
        <property name="n_rows">8</property>
        <property name="n_columns">2</property>
        <child>
          <object class="GtkButton" id="btnstart">
            <property name="label" translatable="yes">Start Process</property>
            <property name="visible">True</property>
            <property name="can_focus">True</property>
            <property name="receives_default">True</property>
            <property name="use_action_appearance">False</property>
          </object>
          <packing>
            <property name="right_attach">2</property>
            <property name="top_attach">6</property>
            <property name="bottom_attach">7</property>
            <property name="y_options">GTK_EXPAND</property>
          </packing>
        </child>
        <child>
          <object class="GtkStatusbar" id="status">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="spacing">2</property>
          </object>
          <packing>
            <property name="right_attach">2</property>
            <property name="top_attach">7</property>
            <property name="bottom_attach">8</property>
          </packing>
        </child>
        <child>
          <object class="GtkLabel" id="lblchoosewav">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="label" translatable="yes">WAV File</property>
            <property name="width_chars">25</property>
            <property name="single_line_mode">True</property>
          </object>
          <packing>
            <property name="top_attach">1</property>
            <property name="bottom_attach">2</property>
          </packing>
        </child>
        <child>
          <object class="GtkLabel" id="lblchoosedir">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="label" translatable="yes">Image Directory</property>
            <property name="width_chars">25</property>
            <property name="single_line_mode">True</property>
          </object>
          <packing>
            <property name="top_attach">2</property>
            <property name="bottom_attach">3</property>
          </packing>
        </child>
        <child>
          <object class="GtkLabel" id="lblchooseout">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="label" translatable="yes">Output File:</property>
            <property name="width_chars">25</property>
            <property name="single_line_mode">True</property>
          </object>
          <packing>
            <property name="top_attach">3</property>
            <property name="bottom_attach">4</property>
          </packing>
        </child>
        <child>
          <object class="GtkLabel" id="lblmain">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="label" translatable="yes">MakeTalk GUI</property>
          </object>
          <packing>
            <property name="right_attach">2</property>
          </packing>
        </child>
        <child>
          <object class="GtkFileChooserButton" id="btnchoosewav">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="title" translatable="yes">Select a WAV</property>
            <property name="width_chars">30</property>
          </object>
          <packing>
            <property name="left_attach">1</property>
            <property name="right_attach">2</property>
            <property name="top_attach">1</property>
            <property name="bottom_attach">2</property>
          </packing>
        </child>
        <child>
          <object class="GtkFileChooserButton" id="btnchoosedir">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="action">select-folder</property>
            <property name="create_folders">False</property>
            <property name="preview_widget_active">False</property>
            <property name="use_preview_label">False</property>
            <property name="title" translatable="yes">Select an image directory</property>
            <property name="width_chars">30</property>
          </object>
          <packing>
            <property name="left_attach">1</property>
            <property name="right_attach">2</property>
            <property name="top_attach">2</property>
            <property name="bottom_attach">3</property>
          </packing>
        </child>
        <child>
          <object class="GtkHBox" id="bboxoutput">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <child>
              <object class="GtkEntry" id="entoutput">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="invisible_char">.</property>
                <property name="width_chars">40</property>
                <property name="primary_icon_activatable">False</property>
                <property name="secondary_icon_activatable">False</property>
                <property name="primary_icon_sensitive">True</property>
                <property name="secondary_icon_sensitive">True</property>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">True</property>
                <property name="position">0</property>
              </packing>
            </child>
            <child>
              <object class="GtkButton" id="btnoutput">
                <property name="label">gtk-save-as</property>
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="receives_default">True</property>
                <property name="use_action_appearance">False</property>
                <property name="use_stock">True</property>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">True</property>
                <property name="padding">6</property>
                <property name="position">1</property>
              </packing>
            </child>
          </object>
          <packing>
            <property name="left_attach">1</property>
            <property name="right_attach">2</property>
            <property name="top_attach">3</property>
            <property name="bottom_attach">4</property>
          </packing>
        </child>
        <child>
          <object class="GtkHBox" id="hbox1">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <child>
              <object class="GtkLabel" id="lblfps">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="label" translatable="yes">Frames Per Second:</property>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">True</property>
                <property name="position">0</property>
              </packing>
            </child>
            <child>
              <object class="GtkEntry" id="entfps">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="invisible_char">.</property>
                <property name="width_chars">4</property>
                <property name="text" translatable="yes">24</property>
                <property name="primary_icon_activatable">False</property>
                <property name="secondary_icon_activatable">False</property>
                <property name="primary_icon_sensitive">True</property>
                <property name="secondary_icon_sensitive">True</property>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">False</property>
                <property name="padding">10</property>
                <property name="position">1</property>
              </packing>
            </child>
          </object>
          <packing>
            <property name="top_attach">4</property>
            <property name="bottom_attach">5</property>
          </packing>
        </child>
        <child>
          <object class="GtkHBox" id="hbox2">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <child>
              <object class="GtkLabel" id="lblsmoothing">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="label" translatable="yes">Smoothing:</property>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">True</property>
                <property name="position">0</property>
              </packing>
            </child>
            <child>
              <object class="GtkEntry" id="entsmoothing">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="invisible_char">.</property>
                <property name="width_chars">4</property>
                <property name="text" translatable="yes">3</property>
                <property name="invisible_char_set">True</property>
                <property name="primary_icon_activatable">False</property>
                <property name="secondary_icon_activatable">False</property>
                <property name="primary_icon_sensitive">True</property>
                <property name="secondary_icon_sensitive">True</property>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">False</property>
                <property name="padding">10</property>
                <property name="position">1</property>
              </packing>
            </child>
            <child>
              <object class="GtkLabel" id="label1">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="label" translatable="yes"> </property>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">True</property>
                <property name="position">2</property>
              </packing>
            </child>
            <child>
              <object class="GtkLabel" id="lblteeth">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="label" translatable="yes"> Teeth Frequency:</property>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">True</property>
                <property name="position">3</property>
              </packing>
            </child>
            <child>
              <object class="GtkEntry" id="entteethfreq">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="invisible_char">.</property>
                <property name="width_chars">4</property>
                <property name="primary_icon_activatable">False</property>
                <property name="secondary_icon_activatable">False</property>
                <property name="primary_icon_sensitive">True</property>
                <property name="secondary_icon_sensitive">True</property>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">True</property>
                <property name="padding">6</property>
                <property name="position">4</property>
              </packing>
            </child>
          </object>
          <packing>
            <property name="left_attach">1</property>
            <property name="right_attach">2</property>
            <property name="top_attach">4</property>
            <property name="bottom_attach">5</property>
          </packing>
        </child>
        <child>
          <object class="GtkHBox" id="hbox3">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <child>
              <object class="GtkLabel" id="lbllowerramp">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="label" translatable="yes">Lower Ramp:</property>
                <attributes>
                  <attribute name="foreground" value="#ffff00000000"/>
                </attributes>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">True</property>
                <property name="position">0</property>
              </packing>
            </child>
            <child>
              <object class="GtkEntry" id="entlowerramp">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="invisible_char">.</property>
                <property name="width_chars">4</property>
                <property name="text" translatable="yes">0</property>
                <property name="invisible_char_set">True</property>
                <property name="primary_icon_activatable">False</property>
                <property name="secondary_icon_activatable">False</property>
                <property name="primary_icon_sensitive">True</property>
                <property name="secondary_icon_sensitive">True</property>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">False</property>
                <property name="padding">10</property>
                <property name="position">1</property>
              </packing>
            </child>
            <child>
              <object class="GtkLabel" id="lblupperramp">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="label" translatable="yes">Upper Ramp:</property>
                <attributes>
                  <attribute name="foreground" value="#ffff00000000"/>
                </attributes>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">True</property>
                <property name="position">2</property>
              </packing>
            </child>
            <child>
              <object class="GtkEntry" id="entupperramp">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="invisible_char">.</property>
                <property name="width_chars">4</property>
                <property name="text" translatable="yes">0</property>
                <property name="invisible_char_set">True</property>
                <property name="primary_icon_activatable">False</property>
                <property name="secondary_icon_activatable">False</property>
                <property name="primary_icon_sensitive">True</property>
                <property name="secondary_icon_sensitive">True</property>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">False</property>
                <property name="padding">10</property>
                <property name="position">3</property>
              </packing>
            </child>
          </object>
          <packing>
            <property name="left_attach">1</property>
            <property name="right_attach">2</property>
            <property name="top_attach">5</property>
            <property name="bottom_attach">6</property>
          </packing>
        </child>
        <child>
          <object class="GtkLabel" id="lblforadvancedusers">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="label" translatable="yes">Ramping for advanced users</property>
            <attributes>
              <attribute name="underline" value="True"/>
              <attribute name="foreground" value="#ffff00000000"/>
            </attributes>
          </object>
          <packing>
            <property name="top_attach">5</property>
            <property name="bottom_attach">6</property>
          </packing>
        </child>
      </object>
    </child>
  </object>
  <object class="GtkFileChooserDialog" id="selectoutdia">
    <property name="can_focus">False</property>
    <property name="border_width">5</property>
    <property name="type_hint">dialog</property>
    <property name="action">save</property>
    <child internal-child="vbox">
      <object class="GtkVBox" id="selectoutdialog">
        <property name="visible">True</property>
        <property name="can_focus">False</property>
        <property name="spacing">2</property>
        <child internal-child="action_area">
          <object class="GtkHButtonBox" id="dialog-action_area1">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="layout_style">end</property>
            <child>
              <object class="GtkButton" id="btndiacancel">
                <property name="label">gtk-cancel</property>
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="receives_default">True</property>
                <property name="use_action_appearance">False</property>
                <property name="use_stock">True</property>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">False</property>
                <property name="position">0</property>
              </packing>
            </child>
            <child>
              <object class="GtkButton" id="btndiasave">
                <property name="label">gtk-save-as</property>
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="receives_default">True</property>
                <property name="use_action_appearance">False</property>
                <property name="use_stock">True</property>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">False</property>
                <property name="position">1</property>
              </packing>
            </child>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="pack_type">end</property>
            <property name="position">0</property>
          </packing>
        </child>
        <child>
          <placeholder/>
        </child>
      </object>
    </child>
    <action-widgets>
      <action-widget response="0">btndiacancel</action-widget>
      <action-widget response="0">btndiasave</action-widget>
    </action-widgets>
  </object>
</interface>
'''

# The GUI class
class maketalkGUI:
	def __init__( self ):
		# Load the gtkbuilder file that was made in glade-gtk
		#self.glade = "maketalk-gtk.ui"
		self.builder = gtk.Builder()
		#self.builder.add_from_file(self.glade)
		self.builder.add_from_string(maketalkguidata)
		# initialize the widgets
		self.mainwindow = self.builder.get_object("mainwindow")
		self.btnstart = self.builder.get_object("btnstart")
		self.btnoutput = self.builder.get_object("btnoutput")
		self.entoutput = self.builder.get_object("entoutput")
		self.entfps = self.builder.get_object("entfps")
		self.entsmoothing = self.builder.get_object("entsmoothing")
		self.entteethfreq = self.builder.get_object("entteethfreq")
		self.selectoutdialog = self.builder.get_object("selectoutdia")
		self.btnchoosewav = self.builder.get_object("btnchoosewav")
		self.btnchoosedir = self.builder.get_object("btnchoosedir")
		self.entlowerramp = self.builder.get_object("entlowerramp")
		self.entupperramp = self.builder.get_object("entupperramp")
		self.status = self.builder.get_object("status")

		# Define some file filters for the file/dir choosing dialogs
		self.wavfilter = gtk.FileFilter()
		self.wavfilter.set_name("Wave Files")
		self.wavfilter.add_pattern("*.wav")
		self.btnchoosewav.add_filter(self.wavfilter)

		self.movfilter = gtk.FileFilter()
		self.movfilter.set_name("MOV Files")
		self.movfilter.add_pattern("*.mov")
		self.selectoutdialog.add_filter(self.movfilter)

		# bind the widgets to commands
		self.btnstart.connect("clicked", self.procvid)
		self.btnoutput.connect("clicked", self.selectoutput)
		self.mainwindow.connect("destroy", self.exit)

		# show the main window
		self.mainwindow.show()

	# The commands to run when widgets clicked etc
	def selectoutput (self, catchall):
		print "select output"
		result = self.selectoutdialog.run()
		theoutfile = self.selectoutdialog.get_filename()
		self.selectoutdialog.hide()
		self.entoutput.set_text(theoutfile)
		print result
		print theoutfile

	def exit(self, catchall):
		gtk.main_quit()

	def procvid (self, catchall):
		self.btnstart.set_sensitive(False)
		self.context_id = self.status.get_context_id("Percent Done")
		if (
			self.btnchoosewav.get_filename() is not None
			and self.entoutput.get_text() is not None
			and self.entsmoothing.get_text() is not None
			and self.entfps.get_text() is not None
			and self.btnchoosedir.get_filename() is not None
		):
			if self.entteethfreq.get_text() is "":
				convcmd = "./maketalk-cl.py -f %s -s %s -w \"%s\" -d \"%s\" -o \"%s\" -r \"%s,%s\"" % (self.entfps.get_text(),self.entsmoothing.get_text(),self.btnchoosewav.get_filename(), self.btnchoosedir.get_filename(),self.entoutput.get_text(),self.entlowerramp.get_text(),self.entupperramp.get_text())
			else:
				convcmd = "./maketalk-cl.py -f %s -s %s -w \"%s\" -d \"%s\" -o \"%s\" -t %s -r \"%s,%s\"" % (self.entfps.get_text(),self.entsmoothing.get_text(),self.btnchoosewav.get_filename(), self.btnchoosedir.get_filename(),self.entoutput.get_text(), self.entteethfreq.get_text(),self.entlowerramp.get_text(),self.entupperramp.get_text())
			print convcmd
			thread = pexpect.spawn(convcmd)
			cpl = thread.compile_pattern_list([pexpect.EOF,'(.+)'])
			while True:
				iconvcmd = thread.expect_list(cpl, timeout=None)
				if iconvcmd == 0: # EOF
					#print "100%"
					break
				elif iconvcmd == 1:
					self.status.push(self.context_id,thread.match.group(0).strip())
					thread.close
				if iconvcmd == 2:
					pass
				while gtk.events_pending():
					gtk.main_iteration()
		self.status.push(self.context_id,"COMPLETE")
		self.btnstart.set_sensitive(True)
	
# main running
if __name__ == "__main__":
	mtg = maketalkGUI()
	gtk.main()
