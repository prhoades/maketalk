##
# maketalk and associated scripts 
# Phillip J Rhoades http://howtophil.com
# The lip sync animation code from Silas Brown http://linuxgazette.net/181/brownss.html was a great start
# Gaussian data smoothing code from Scott W Harden http://www.swharden.com/blog/2008-11-17-linear-data-smoothing-in-python/
##

maketalk-cl.py is a python script which leverages sox, mencoder, avconv and a few other back end programs to create rough-but-good-enough lip sync animation based on the amplitudes of a WAV file with some Gaussian blur applied to the data to smooth mouth movement. It can also be used to animate other things that you would like to sync up with a sound file (I have used it to animate the flames coming out of a rocket so they moved in concert with the "rocket" sound file I was using). 

Output is as an alpha channel preserving Quicktime MOV. The benefit of using the Quicktime MOV is that you can then composite the lips onto a character face in Kdenlive or any other advanced video editing program (much like old "talking head" limited animation cartoons). 

The GUI (maketalk-gui.py) is a very simple form that allows you to enter in a directory that contains ONLY the images you're going to use as mouth movement frames, a wav file, an output file, and adjust the frame rate and smoothing. Currently it does NOTHING TO PROTECT YOU FROM YOURSELF - so be careful. In fact the same goes for the command line program. I have not yet built in safety. So if you tell it to overwrite a file - it WILL overwrite a file. Know what you are doing and have fun. :)

The "ramping" option allows you to fudge the volume data so that the loudest or quietest section of a volume slice is moved towards a neighboring frame. It's useful for noisy or odd distribution of volumes wav files. It's for advanced users. 

Example commands:
	To Start the Graphical User Interface (GUI)
		# ./maketalk-gui.py

	Basic Command Line
		# ./maketalk-cl.py -d ./bald -o terrible_old_man_test -w ./terrible_old_man_test.wav

	With "ramping" (moving) the top 10 percent of the lowest slice and the lowest 10 percent of the upper slice towards their nearest neighbor frames.
		# ./maketalk-cl.py -r "10,10" -d ./bald -o terrible_old_man_test -w ./terrible_old_man_test.wav

	With "ramping" (moving) the bottom 10 percent of the lowest+1 slice towards the minimum lip frame and the highest 10 percent of the upper-1 slice towards the maximum lip frame
		# ./maketalk-cl.py -r "-10,-10" -d ./bald -o terrible_old_man_test -w ./terrible_old_man_test.wav

	if you have a teeth.png in your image directory you can specify a base frequency and anything above that will toss out a teeth frame
		# ./maketalk-cl.py -s3 -d ./bald -o terrible_old_man_test -w ./terrible_old_man_test.wav -t 2000

